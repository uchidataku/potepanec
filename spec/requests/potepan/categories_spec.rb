require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "GET /potepan/categories" do
    let!(:taxonomy) { create(:taxonomy, name: "Categories") }
    let!(:taxon1)   { create(:taxon, name: "Bags", taxonomy: taxonomy) }
    let!(:taxon2)   { create(:taxon, name: "Mugs", taxonomy: taxonomy) }
    let!(:product)  { create(:product, name: "TOTE", taxons: [taxon1]) }

    before do
      get potepan_category_path(taxon1.id)
    end

    it "正常にレスポンスを返す" do
      expect(response).to be_successful
    end

    it "正常に@taxonomiesが表示される" do
      expect(response.body).to include "Categories"
    end

    it "正常に@taxonが表示される" do
      expect(response.body).to include "Bags"
    end

    it "正常に@productが表示される" do
      expect(response.body).to include "TOTE"
    end

    it "選択したtaxonに紐付かない商品が表示されない" do
      get potepan_category_path(taxon2.id)
      expect(response.body).not_to include "TOTE"
    end
  end
end
