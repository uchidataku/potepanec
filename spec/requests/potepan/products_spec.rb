require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe "GET /potepan/products" do
    let(:product) { create(:product, name: "sample", price: "12.34", description: "description of sample") }

    before do
      get potepan_product_path(product.id)
    end

    it "正常にレスポンスを返す" do
      expect(response).to be_successful
    end

    it "正常にテンプレートを返す" do
      expect(response).to render_template :show
    end

    it "正常に商品名が表示される" do
      expect(response.body).to include("sample")
    end

    it "正常に商品価格が表示される" do
      expect(response.body).to include("12.34")
    end

    it "正常に商品説明が表示される" do
      expect(response.body).to include("description of sample")
    end

    it "@productが期待される値を持っている" do
      expect(assigns(:product)).to eq product
    end

    it "正常にタイトルが表示される" do
      get potepan_product_path(product.id)
      assert_select "title", "#{product.name} | BIGBAG Store"
    end
  end
end
