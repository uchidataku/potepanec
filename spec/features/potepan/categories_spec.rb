require 'rails_helper'

RSpec.feature "Potepan::Categories", type: :feature do
  let!(:taxonomy) { create(:taxonomy) }
  let!(:taxon1)   { create(:taxon, name: "Bags", taxonomy: taxonomy) }
  let!(:taxon2)   { create(:taxon, name: "Mugs", taxonomy: taxonomy) }
  let!(:product)  { create(:product, name: "TOTE", taxons: [taxon1]) }

  before do
    visit potepan_category_path(taxon1.id)
  end

  scenario "正常にタイトルが表示される" do
    expect(page).to have_title "#{taxon1.name} | BIGBAG Store"
    within ".page-title" do
      expect(page).to have_content taxon1.name
    end
  end

  scenario "正常にサイドバーが表示される" do
    within ".side-nav" do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content "#{taxon1.name} (#{taxon1.products.count})"
      expect(page).to have_content "#{taxon2.name} (#{taxon2.products.count})"
    end
  end

  scenario "正常にカテゴリー別に商品名・値段が表示される" do
    click_link "#{taxon1.name} (#{taxon1.products.count})"
    expect(current_path).to eq potepan_category_path(taxon1.id)
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
  end

  scenario "選択したカテゴリーに紐付かない商品名・値段が表示されない" do
    click_link "#{taxon2.name} (#{taxon2.products.count})"
    expect(current_path).to eq potepan_category_path(taxon2.id)
    expect(page).not_to have_content product.name
    expect(page).not_to have_content product.display_price
  end

  scenario "正常に商品名から商品詳細ページへ移動される" do
    click_link product.name
    expect(current_path).to eq potepan_product_path(product.id)
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
  end
end
